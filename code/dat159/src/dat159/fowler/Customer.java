package dat159.fowler;

import java.util.Enumeration;
import java.util.Vector;

public class Customer {
    private String _name;
    private Vector _rentals = new Vector();

    public Customer(String name) {
        _name = name;
    }

    public String statement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;
        Enumeration rentals = _rentals.elements();
        String result = getRentalRecords(getName());
        Movie movie;
        int daysRented;
        double thisAmount;
        while (rentals.hasMoreElements()) {
            thisAmount = 0;
            Rental each = (Rental) rentals.nextElement();
            daysRented = each.getDaysRented();
            movie = each.getMovie();
            
            // determine amount for each line
            thisAmount = movie.getMoviePrice(daysRented);

            // add frequent renter points
            frequentRenterPoints ++;
            // add bonus for a two day new release rental
            if (movie.isNewRelease() && daysRented > 1) frequentRenterPoints ++;

            //show figures for this rental
            result += getFiguresPrinting(movie.getTitle(), thisAmount);
            totalAmount += thisAmount;
        }
        result += getFooterLines(totalAmount, frequentRenterPoints);
        return result;
    }
    
    public String getFooterLines(double totalAmount, int frequentRenterPoints) {
    	String help = "Amount owed is " + String.valueOf(totalAmount) + "\n";
    	help += "You earned " + String.valueOf(frequentRenterPoints) +
                " frequent renter points";
    	return help;
    }

    public String getRentalRecords(String name) {
    	return "Rental Record for " + name + "\n";
    }
    
    public String getFiguresPrinting(String movie, double amount) {
    	return "\t" + movie + "\t" +
                String.valueOf(amount) + "\n";
    }
    
    public void addRental(Rental arg) {
        _rentals.addElement(arg);
    }

    public String getName() {
        return _name;
    }
}
