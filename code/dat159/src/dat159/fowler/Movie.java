package dat159.fowler;

abstract public class Movie {

    private String _title;
    private int _priceCode;

    public Movie(String title, int priceCode) {
        _title = title;
        _priceCode = priceCode;
    }

    public int getPriceCode() {
        return _priceCode;
    }

    public void setPriceCode(int _priceCode) {
        this._priceCode = _priceCode;
    }

    public String getTitle() {
        return _title;
    }
    
    abstract double getMoviePrice(int daysRented);
    
    abstract boolean isNewRelease();
    
    public class Childrens extends Movie {

    	public Childrens(String title, int priceCode) {
    		super(title, priceCode);
    		// TODO Auto-generated constructor stub
    	}
    	
    	public double getMoviePrice(int daysRented) {
    		double amount = 1.5;
    		if (daysRented > 3)
                amount += (daysRented - 3) * 1.5;
			return amount;
    	}
    	
    	public boolean isNewRelease() {
			return false;
		}
    }
    
    public class Regular extends Movie {

		public Regular(String title, int priceCode) {
			super(title, priceCode);
			// TODO Auto-generated constructor stub
		}
    	
		public double getMoviePrice(int daysRented) {
    		double amount = 2;
    		if (daysRented > 2)
                amount += (daysRented - 2) * 1.5; //change
			return amount;
    	}
		
		public boolean isNewRelease() {
			return false;
		}
    }
    
    public class NewRelease extends Movie {

		public NewRelease(String title, int priceCode) {
			super(title, priceCode);
			// TODO Auto-generated constructor stub
		}
    	
		public double getMoviePrice(int daysRented) {
    		double amount = daysRented * 3;
			return amount;
    	}
		
		public boolean isNewRelease() {
			return true;
		}
    }
}

