package dat159.refactoring;

/* Knuth's PrintPrimes / Clean Code */

public class PrintPrimes {
	
  private static int M = 1000;
  private static int P[] = new int[M + 1];
	
  public static void main(String[] args) {
    
    final int RR = 50;
    final int CC = 4;
    final int WW = 10;
    final int ORDMAX = 30;
    int J;
    int K;
    boolean JPRIME;
    int ORD;
    int SQUARE;
    int N;
    int MULT[] = new int[ORDMAX + 1];
    J = 1;
    K = 1;
    P[1] = 2;
    ORD = 2;
    SQUARE = 9;

    while (K < M) {
      do {
        J = J + 2;
        if (J == SQUARE) {
          ORD += 1;
          SQUARE = P[ORD] * P[ORD];
          MULT[ORD - 1] = J;
        }
        N = 2;
        JPRIME = true;
        while (N < ORD && JPRIME) {
          while (MULT[N] < J)
            MULT[N] += P[N] + P[N];
          if (MULT[N] == J)
            JPRIME = false;
          N += 1;
        }
      } while (!JPRIME);
      K += 1;
      P[K] = J;
    }
    {
      int PAGENUMBER = 1;
      int PAGEOFFSET = 1;
      int ROWOFFSET;
      int C;
      while (PAGEOFFSET <= M) {
        printOutputHeader(PAGENUMBER);
        for (ROWOFFSET = PAGEOFFSET; ROWOFFSET < PAGEOFFSET + RR; ROWOFFSET++){
          for (C = 0; C < CC;C++)
            if (ROWOFFSET + C * RR <= M)
              System.out.format("%10d", P[ROWOFFSET + C * RR]);
          System.out.println("");
        }
        System.out.println("\f");
        PAGENUMBER ++;
        PAGEOFFSET += RR * CC;
      }
    }
  }
  
  static void printOutputHeader(int pagenumber) {
	  System.out.println("The First " + M +
              " Prime Numbers --- Page " + pagenumber);
	  System.out.println("");
  }
  
  static void printPrimes() {
	  
  }
}